import { Property } from "../domain/models/property";
import {LoggingService} from "../services/LoggingService";
const debug = require("debug")("controller");

export class PropertyController {
  private property: Property;

  constructor(property: Property) {
    this.property = property;
  }

  async handleHeating() {
    try {
      const homeUpdates = this.property.homes.map(home => home.update());
      await Promise.all(homeUpdates);
      const activeHeatingRequest = this.property.homes.some(
        home => home.heatingRequest
      );
      this.property.boiler.heatingControl(activeHeatingRequest);
    } catch (e) {
      debug('Error');
      LoggingService.error(e);
    }
  }

  handleWatter() {}

  handleElectricity() {}
}
