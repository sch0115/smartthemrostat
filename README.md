Smart thermostat je program, který spojuje API firmy Netatmo, se kterým
komunikují termohlavice nainstalované na radiátorech v domě a kotel.
Termohlavice neumi komunikovat primo s kotlem, proto jsem na kotel 
napojil relé od firmy Sonoff a vytvořil tento program, který cte
data z hlavic zkrz API a odesílá pokyny pro topení pomocí MQTT.


to install all dependencies run:
$ npm install


to compile TypeScript to JavaScript run:
$ tsc


to start the programm run:
$ node index.js


Program uses "debug" library which can be used to see 
output from the program in console. 
To enable this output set enviroment variable:

DEBUG=*,-databaseMessages