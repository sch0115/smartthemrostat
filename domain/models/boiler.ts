import { Home } from "./home";
import { IRelay } from "../interfaces/IRelay";
import { LoggingService } from "../../services/LoggingService";
let mongoose = require("mongoose");
const debug = require("debug")("boiler");
const databaseDebug = require("debug")("databaseMessages");

export class Boiler {
  id: String;
  homesToHeat: Array<Home>;
  boilerControlRelay: IRelay;
  isHeating: Boolean;

  constructor(
    id: String,
    homesToHeat: Array<Home>,
    boilerControlRelay: IRelay
  ) {
    this.id = id;
    this.homesToHeat = homesToHeat;
    this.boilerControlRelay = boilerControlRelay;
    this.isHeating = boilerControlRelay.isOn();
  }

  heatingControl(heatingOn: Boolean): void {
    debug("heating control", heatingOn);
    heatingOn ? this.boilerControlRelay.on() : this.boilerControlRelay.off();
    this.isHeating = heatingOn;
    // this.save()
  }

  async save(): Promise<void> {
    try {
      const boilerModel = new boilerDatabaseModel({
        ...this,
        createdAt: new Date()
      });
      return boilerModel
        .save()
        .then(doc => databaseDebug(doc))
        .catch(err => LoggingService.error(err));
    } catch (err) {
      LoggingService.error(err);
    }
  }
}

const boilerSchema = new mongoose.Schema({
  id: {
    type: String,
    required: true
  },
  entityType: {
    type: String,
    default: "boiler"
  },
  createdAt: {
    type: Date,
    required: true
  },
  isHeating: {
    type: Boolean,
    required: true
  }
});

const boilerDatabaseModel = mongoose.model("Boiler", boilerSchema);
