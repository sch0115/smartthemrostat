import { LoggingService } from "../../services/LoggingService";

const netatmo = require("netatmo");
const debug = require("debug")("netatmo");
const mongoose = require("mongoose");
const databaseDebug = require("debug")("databaseMessages");

export class Home {
  id: string;
  api: any;
  rooms: Array<any>;
  modules: Array<object>;
  heatingRequest: boolean;
  private lastUpdate: Date;
  private readonly description: string;

  constructor(auth: object, homeId: string, description: string) {
    this.id = homeId;
    this.api = new netatmo(auth);
    this.description = description;
    this.lastUpdate = null;
    debug('home ', description, ' created');
  }

  async update() {
    debug("home " + this.description + " update");
    const netatmoEnergyHome = await this.getNetatmoEnergyHome(this.id);
    this.modules = netatmoEnergyHome.modules;
    this.rooms = netatmoEnergyHome.rooms;
    this.heatingRequest = this.rooms.some(room => room.heating_power_request);
    this.lastUpdate = new Date();
    this.save();
  }

  async getNetatmoEnergyHome(home_id): Promise<any> {
    return new Promise((resolve, reject) => {
      this.api.getHomeStatus({ home_id }, function(err, response: any) {
        if (err) reject(err);
        resolve(response.home);
      });
    });
  }

  async save() {
    try {
      const homeModel = new homeDatabaseModel({ ...this });
      return homeModel
        .save()
        .then(doc => databaseDebug(doc))
        .catch(e => LoggingService.error(e));
    } catch (e) {
      LoggingService.error(e);
    }
  }
}

const homeSchema = new mongoose.Schema({
  id: {
    type: String,
    required: true
  },
  entityType: {
    type: String,
    default: "home"
  },
  createdAt: {
    type: Date,
    default: new Date()
  },
  rooms: {
    type: Array,
    required: true
  },
  modules: {
    type: Array,
    required: true
  },
  heatingRequest: {
    type: Boolean,
    required: true
  }
});

const homeDatabaseModel = mongoose.model("Home", homeSchema);
