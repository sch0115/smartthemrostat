// ===================================================================
// RELAY module
// Parameters: GPIO, isClosed, current
//Methods: On, Off, isOn, getState
// var Gpio = require('onoff').Gpio;
// @ts-ignore

import {Gpio} from 'onoff';
import {IRelay} from "../../interfaces/IRelay";

export class Relay implements IRelay{
    private description: string;
    private gpio: number;
    private current: number;
    private closed: boolean;
    private onSignal: number;
    private offSignal: number;
    private connection: any;
  constructor(gpio, isClosed, current, description) {
      this.gpio = gpio;
      this.current = current;
      this.closed = isClosed;
      this.description = description;
      this.onSignal = this.closed ? 1 : 0;
      this.offSignal = this.closed ? 0 : 1;
      this.connection = new Gpio(gpio, 'out');
  }

  isOn(): boolean {
    return this.connection.readSync() === this.onSignal;
  }

  on(): void {
    this.connection.writeSync(this.onSignal);
  }
  off(): void {
    this.connection.writeSync(!this.onSignal);
  }
  getState(): string {
    return 'RELAY on port ' + this.gpio + ' is ' + this.isOn() ? 'ON' : 'OFF';
  }
  public getDescription() : string {
    return this.description;
  }
  destroy(): void {
    this.connection.unexport();
  }
}
