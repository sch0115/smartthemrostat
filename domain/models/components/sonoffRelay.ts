import { IRelay } from "../../interfaces/IRelay";
import { MQTTService } from "../../../services/MQTTService";
import { LoggingService } from "../../../services/LoggingService";

export class SonoffRelay implements IRelay {
  private description: string;
  private topic: any;
  private mqttService: MQTTService;
  private isClosed: boolean;

  constructor(mqttTopic, mqttService: MQTTService, description) {
    this.description = description;
    this.topic = mqttTopic;
    this.mqttService = mqttService;
  }

  async connect(): Promise<void> {
    await this.mqttService.subscribe("stat/" + this.topic, (topic, payload) => {
      this.update(payload.toString());
    });
    this.isClosed = await this.mqttService.send(
      "cmnd/" + this.topic,
      "check",
      "stat/" + this.topic
    );
  }

  isOn(): boolean {
    return this.isClosed;
  }

  on(): void {
    this.mqttService
      .send("cmnd/" + this.topic, "ON")
      .catch(e => LoggingService.error(e));
  }

  off(): void {
    this.mqttService
      .send("cmnd/" + this.topic, "OFF")
      .catch(e => LoggingService.error(e));
  }

  update(state): void {
    this.isClosed = state === "ON";
  }

  async getState(): Promise<any> {
    return await this.mqttService.send(
      "cmnd/" + this.topic,
      "check",
      "stat/" + this.topic
    );
  }

  public getDescription(): string {
    return this.description;
  }
}
