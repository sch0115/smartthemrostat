// @ts-ignore
import {DHT11} from 'rpi-dht-sensor';

export class DHT {
    private Gpio: any;
    private connection: DHT11;
    private readout: any;
    private lastUpdate: number;
    private description: any;

    constructor(Gpio, description) {
        this.Gpio = Gpio;
        this.description = description;
        this.readout = null;
        this.lastUpdate = null;
        this.connection = new DHT11(2);
    }

    async getTemperature() {
        const readout = await this._read();
        return readout.temperature.toFixed(2);
    }

    async getHumidity() {
        const readout = await this._read();
        return readout.humidity.toFixed(2);
    }

    getDescription() {
        return this.description;
    }

    getState() {
        return 'DHT11 on port ' + this.Gpio + ' reads ' + this.getTemperature() + 'C ' + this.getHumidity() + '% humidity';
    }

    destroy() {
        console.log('Destroy of DHT - Not Implemented Yet');
    }

    async _read() {
        if (this.lastUpdate < Date.now() - 60) {
            this.readout = await this.connection.read();
        }
        return this.readout;
    }
};
