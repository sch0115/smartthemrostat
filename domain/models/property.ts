import {Home} from "./home";
import {Boiler} from "./boiler";

export class Property {
    homes: Array<Home>;
    boiler: Boiler;
    private description: string;
    
    constructor(homes: Array<Home>, boiler: Boiler, description: string) {
        this.homes = homes;
        this.boiler = boiler;
        this.description = description; 
    }
}