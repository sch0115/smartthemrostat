export interface IThermometer {
    getTemperature(): number
}