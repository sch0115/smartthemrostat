export interface IRelay {
    isOn(): boolean

    on(): void

    off(): void

    getState(): any

    getDescription(): string
}
