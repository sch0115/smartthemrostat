import { WriteStream } from "fs";

const debug = require("debug")("FileSystemService");
const fs = require("fs");

export class FileSystemService {
  private static currentOpenFile: any;
  private static logger: WriteStream;
  constructor() {}
  static writeFile = async (path, content) => {
    try {
      if (FileSystemService.currentOpenFile === path) {
        FileSystemService.logger.write(content);
      } else {
        FileSystemService.logger && FileSystemService.logger.end();
        FileSystemService.logger = fs.createWriteStream(path, { flags: "a" });
        FileSystemService.logger.write(content);
      }
      debug("The file " + path + " was updated!");
    } catch (err) {
      debug("ERROR writing to file", err);
    }
  };
}

// export class FileSystemService {
//   constructor() {}
//   static writeFile = async (path, content) => {
//     fs.appendFile(path, content, err => {
//       err
//           ? debug("ERROR writing to file", err)
//           : debug("The file " + path + " was updated!");
//     });
//   };
// }
