import { MqttClient } from "mqtt";
import { LoggingService } from "./LoggingService";

const mqtt = require("mqtt");
const debug = require("debug")("MQTT client");
const debugMqttMessage = require("debug")("MQTT message");

export class MQTTService {
  private readonly brokerUrl: string;
  private client: MqttClient;
  private readonly subscribedTopics: any;

  constructor(brokerUrl: string) {
    this.brokerUrl = brokerUrl;
    this.subscribedTopics = {};
  }

  async getClient(): Promise<MqttClient> {
    if (this.client) return this.client;
    return await new Promise((resolve, reject) => {
      debug("Connecting to MQTT client...");
      const client = mqtt.connect(this.brokerUrl);
      const timeout = setTimeout(() => {
        LoggingService.error("timeout fail");
        reject("MQTT connection commandTimeout");
      }, 250);
      client.on("connect", () => {
        debug("MQTT client connected");
        this.client = client;
        clearTimeout(timeout);
        this.client.on("message", () => this.handleMessage);
        resolve(this.client);
      });
    });
  }

  handleMessage = (topic: string, payload: any): void => {
    debug("message received");
    debugMqttMessage(payload.toString(), ...Object.keys(this.subscribedTopics));
    for (const subscribedTopic of Object.keys(this.subscribedTopics)) {
      if (subscribedTopic === topic) {
        for (const callback of this.subscribedTopics[subscribedTopic]) {
          callback(topic, payload);
        }
      }
    }
  };

  async subscribe(
    topic: string,
    callback: (topic: string, payload: any) => void
  ) {
    const client = await this.getClient().catch(e => LoggingService.error(e));
    if (client instanceof MqttClient) {
      client.subscribe(topic);
      let topicRecord = this.subscribedTopics[topic];
      topicRecord
        ? this.subscribedTopics[topic].push(callback)
        : (this.subscribedTopics[topic] = [callback]);
      debug("Subscribed topics: ", this.subscribedTopics);
    } else {
      LoggingService.error("Subscription failed");
    }
  }

  // async unsubscribe(topic: string, callback: (topic: string, payload: any) => void) {
  //     this.client.subscribe(topic)
  //     let topicRecord = this.subscribedTopics[topic]
  //     if(topicRecord.lenght > 1 && )
  //     topicRecord = topicRecord && topicRecord.lenght ? topicRecord.remove(callback) : topicRecord = [callback]
  // } TODO implement unsubscribe (no need to use yet...)

  async send(
    topic: string,
    payload: any,
    responseTopic?: string
  ): Promise<any> {
    const client = await this.getClient().catch(e => LoggingService.error(e));
    if (!(client instanceof MqttClient)) return;
    if (responseTopic) {
      client.subscribe(responseTopic);
      client.publish(topic, payload);
      return await new Promise((resolve, reject) => {
        const timeout = setTimeout(() => {
          LoggingService.error(
            "MQTT send commandTimeout",
            topic,
            payload,
            responseTopic
          );
          reject("MQTT send commandTimeout");
        }, 250);
        client.on("message", function(topic, message) {
          debugMqttMessage('response: ', message.toString());
          clearTimeout(timeout);
          client.unsubscribe(responseTopic);
          resolve(message.toString());
        });
      }).catch(e => {
        LoggingService.error(e);
      });
    }
    client.publish(topic, payload);
  }
}
