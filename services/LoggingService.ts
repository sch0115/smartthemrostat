import {FileSystemService} from "./FileSystemService";

const debug = require("debug")("LoggingService");

export class LoggingService {
  constructor() {}
  static error(err: string, ...args) {
    debug("ERROR: ", err, ...args);
    const date = new Date();
    const filePath = 'error_log_' + date.getDate() + date.getMonth()+1 + date.getFullYear() + '.txt';
    const errorRecord = new Date() + ' => ' + err;
    FileSystemService.writeFile(filePath, errorRecord + '\n');
  }
}
