import {LoggingService} from "./services/LoggingService";

let mongoose = require('mongoose');
const database = 'Netatmo';
const debug = require('debug')('database');

export class Database {
    constructor() {
    }

    static connect() {
        debug('connecting to database...');
        mongoose.connect(
            "mongodb+srv://sch0115:Passw0rd01@cluster0-sih4m.mongodb.net/" + database + "?retryWrites=true",
            {useNewUrlParser: true, useUnifiedTopology: true}
        ).then(() => {
            debug('Database connection successful');
        }).catch(err => {
            LoggingService.error('Database connection error', err);
            setTimeout(this.connect, 60000);
        })
    }
}