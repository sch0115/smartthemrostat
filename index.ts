import {PropertyController} from "./controllers/propertyController";
import {SonoffRelay} from "./domain/models/components/sonoffRelay";
import {MQTTService} from "./services/MQTTService";
import {Property} from "./domain/models/property";
const debug = require('debug')('app');
const {Boiler} = require("./domain/models/boiler");
const {Home} = require("./domain/models/home");
const {Database} = require('./database');

const auth = {
    "client_id": "5e04d07f216130c23e5f1c5c",
    "client_secret": "OmODiGzteEHZ2DnWArHFH2Anpcq17O5aAA5",
    "username": "adam.sch0115@gmail.com",
    "password": "Mustang23!",
};


const polni2bytA = new Home(auth, '5e03479867368a14aa243b53', 'Roubenka');
const polni2bytC = new Home(auth, '5e19e54222ab74eb923506ec', 'Polni2C');
const brokerUrl = 'mqtt://192.168.68.115';

const mqttService = new MQTTService(brokerUrl);
const relay = new SonoffRelay('basement/POWER2', mqttService, "Boiler relay controlling heating");
const boiler = new Boiler('001', [polni2bytA, polni2bytC], relay);
const homes = [polni2bytA, polni2bytC];
const property = new Property(homes, boiler, 'Polni 309/2');
const propertyController = new PropertyController(property);

let cycles = 0;
const cycle = () => {
    debug(new Date(), '=====================================', ' cycle ', cycles++ + 1);
    propertyController.handleHeating();
    propertyController.handleWatter();
    propertyController.handleElectricity();
};

const init = async () => {
    Database.connect();
    await relay.connect();
};

// Start of the program //
debug('started');
init().then(() => {
    cycle();
    setInterval(cycle, 60000);
});

