import { MQTTService } from "./services/MQTTService";
import { SonoffRelay } from "./domain/models/components/sonoffRelay";

const brokerUrl = "mqtt://192.168.68.115";

async function test() {
  const mqttService = new MQTTService(brokerUrl);
  // const testResult = await mqttService.send('cmnd/sonoff4ch/power1', 'ON', 'stat/sonoff4ch/POWER1' );
  const sonoffRelay = new SonoffRelay(
    "sonoff4ch/POWER1",
    mqttService,
    "Test sonoff relay"
  );
  await sonoffRelay.connect();
  sonoffRelay.off();
  setInterval(() => {
    const currentState = sonoffRelay.isOn();
    currentState ? sonoffRelay.off() : sonoffRelay.on();
    setTimeout(() => {
      currentState ? sonoffRelay.on() : sonoffRelay.off();
    }, 500);
  }, 13000);

  // console.log(testResult)
}

test();
